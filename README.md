# [Hugo](http://gohugo.io/) Theme: Lucid

Features

- Support for Blog and Quick Tip style content
- Support for tag pages and custom page content for each tag
- Built with the [Bulma.io](https://bulma.io/) CSS framework
- Support for SASS/SCSS
- Icon support with [ForkAwesome](https://github.com/ForkAwesome/Fork-Awesome)
- No cookies
- Optional website analytics using [GoatCounter](https://www.goatcounter.com)

## Installation

To install the theme you will add the theme contents as a git submodule. Since the theme also uses submodules for other components you will then need to tell git to update all nested submodules. The easiest way to do that is with `git submodule update --init --recursive`.

Inside the folder of your Hugo site run:

```
git submodule add https://gitlab.com/hectorjsmith/hugo-theme-lucid.git themes/hugo-theme-lucid
git submodule update --init --recursive
```

Next, open `config.toml` in the base of the Hugo site and ensure the theme option is set to `hugo-theme-lucid`.

```toml
theme = "hugo-theme-lucid"
```

Finally, add the following parameters to your `config.toml` file.

```toml
paginate = 2

[params]
  author = "The default author"
  locale = "en_UK"
  lang = "en"
  wordsPerMinute = 200
```

## Blog

This theme includes support for rendering blog posts. The main blog page is available at `/blog`.

The content of the `content/blog/_index.md` file will be rendered above the list of blog posts on the main blog page.

Blog posts created under the `content/blog` folder will automatically be included in the main blog page. See the included `archetypes/post.md` for the available fields for blog posts.

## Tips

Very similar to blog posts, but designed to be smaller. Tips include a featured image that is used when showing a list of tips on a single page.

## Tags

Blog posts and Tips can both have any number of tags. By default tags are displayed below the content when viewing a post.

Clicking on a tag takes the user to the tag page, where all posts (blog posts or tips) with that tag are displayed.

To add custom content for a tag, create a file at the following location:

```
content/tags/<tag name>/_index.md
```

From there you can add custom markdown content for that tag page as well as customize the page using properties in the frontmatter.

## Analytics

This theme has support for website analytics using [GoatCounter](https://www.goatcounter.com).
It is possible to use either the hosted version or a self-hosted instance.

The theme will include the required javascript on all pages when analytics has been enabled.

To enable analytics, add the following parameters to the site `config.toml` file.

```toml
[params]
  analyticsEnabled = true
  goatcounterUrl = "https://url.for.goatcounter"
```

A short message is added to the bottom of every page when analytics are enabled.

Note that analytics are **disabled** if any of the following is true:
* These options are not found in the config file
* The `analyticsEnabled` is set to `false`
* The `goatcounterUrl` is blank

## Custom SASS/SCSS

This theme makes use of `sass`/`scss` styles and therefore requires the use of `hugo-extended`.

To add custom `sass`/`scss` styles simply create a new file at `assets/scss/custom.scss` for your site and put changes there. This file will overwrite the one included in the theme and will be automatically included in the build.

Note: You will need to restart `hugo` to pick-up the file override.

### Customizing Bulma CSS

This theme uses the [Bulma.io](https://bulma.io/) CSS framework for styling and supports easy customization of bulma parameters by adding them to the `assets/scss/custom.scss` file.

See the [documentation](https://bulma.io/documentation/customize/) to learn more about what you can change in Bulma.

## Example

This theme includes an example site under `example/` that shows off some of the functionalities of the theme. To use the example site, run `hugo` from inside the `example` folder.

The latest version of the example site is automatically built and served as a [static site](https://hectorjsmith.gitlab.io/hugo-theme-lucid).

## Production

To run in production run `HUGO_ENV=production` before your build command. For example:

```
HUGO_ENV=production
hugo
```

Or, if you are running windows:

```
set HUGO_ENV=production
hugo
```

## Contributing

Feel free to contribute any ideas or bugs you find in the project's [issue tracker](https://gitlab.com/hectorjsmith/hugo-theme-lucid/-/issues).
