function scrollToIdSmoothly(id) {
  var element = document.getElementById(id);
  element.scrollIntoView({
    behavior: 'smooth'
  });
}
