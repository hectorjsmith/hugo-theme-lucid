---
title: "{{ replace .Name "-" " " | title }}"
subtitle: ""
description:
date: {{ .Date }}
draft: true
tags: []
featureImg:
  url:
  alt:
  credit: ""
  source: ""
hero:
  size: halfheight
  background: primary
  titleFg:
  embedTitle: true
  image:
    show: false
    url:
navBg:
navFg: light
author:
---
