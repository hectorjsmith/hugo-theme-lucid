---
title: About
subtitle: Everything you need to know about me
featureImg:
  url: "/img/home-img.jpg"
hero:
  size: medium
  embedTitle: false
  image:
    show: true
    url: "/img/home-bg.jpg"
navFg: dark
---


{{< section/left-image-with-title image="/img/home-img.jpg" title="Short Version" >}}
Lid est laborum et dolorum fuga. Et harum quidem rerum facilis est et expeditasi distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihilse impedit quo minus id quod amets untra dolor amet sad.
{{< /section/left-image-with-title >}}

{{< social/plain-social-links >}}

----

## Long Version

Some sample text that goes into more detail about me.

Lid est laborum et dolorum fuga. Et harum quidem rerum facilis est et expeditasi distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihilse impedit quo minus id quod amets untra dolor amet sad. Sed ut perspser iciatis unde omnis iste natus error sit voluptatem accusantium doloremque laste. Dolores sadips ipsums sits.

Lid est laborum et dolorum fuga. Et harum quidem rerum facilis est et expeditasi distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihilse impedit quo minus id quod amets untra dolor amet sad. Sed ut perspser iciatis unde omnis iste natus error sit voluptatem accusantium doloremque laste. Dolores sadips ipsums sits.
