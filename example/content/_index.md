---
title: John Doe
subtitle: Some more useful information about me. This serves as a simple intro to who I am and why you should read this blog.
layout: home
hideTitleInHead: true
featureImg:
  url: "/img/home-img.jpg"
hero:
  titleFg: dark
  image:
    show: true
    url: "/img/home-bg.jpg"
navFg: dark
---

## What is this blog for?

This blog is for:
* This
* That
* And some of this

And finally, some other stuff

## Why did I create this website?

* This reason
* That reason
