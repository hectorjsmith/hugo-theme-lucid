---
title: "Short Post 2 with a longer title to see wrapping"
date: 2020-08-21T21:54:27+01:00
tags: [ "CSS" ]
featureImg:
  url: "/img/laptop.jpg"
  alt: Laptop
hero:
  background: info
  size: halfheight
  embedTitle: true
navFg: light
---

This is the second short post
