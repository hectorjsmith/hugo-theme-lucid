---
title: "Short Post 1"
date: 2020-08-21T21:54:27+01:00
subtitle: "My First Short Post"
tags: [ "Hugo", "CSS" ]
featureImg:
  url: "/img/laptop.jpg"
  alt: Laptop
navBg: info
navFg: light
author: John Doe
---

This is the first short post
