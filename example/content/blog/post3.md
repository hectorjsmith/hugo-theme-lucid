---
title: "Post3"
date: 2020-08-21T21:54:27+01:00
subtitle: "My Third Post"
tags: [ "Hugo", "Sample" ]
featureImg:
  url: "/img/laptop.jpg"
  credit: "XPS"
  source: "https://unsplash.com/photos/yNvVnPcurD8"
hero:
  size: halfheight
  embedTitle: false
  image:
    show: true
navFg: light
---

Some preamble text to get some context before we start.

Lid est laborum et dolorum fuga. Et harum quidem rerum facilis est et expeditasi distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihilse impedit quo minus id quod amets untra dolor amet sad. Sed ut perspser iciatis unde omnis iste natus error sit voluptatem accusantium doloremque laste. Dolores sadips ipsums sits.

## Post Number 3

This is the first post

Lid est laborum et dolorum fuga. Et harum quidem rerum facilis est et expeditasi distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihilse impedit quo minus id quod amets untra dolor amet sad. Sed ut perspser iciatis unde omnis iste natus error sit voluptatem accusantium doloremque laste. Dolores sadips ipsums sits.
