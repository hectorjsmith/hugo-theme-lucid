---
title: "Markdown Sample"
date: 2020-08-01T08:23:38+01:00
subtitle: "A post used to showcase some of Markdown's features"
tags: [ "Markdown", "Sample" ]
featureImg:
  url: "/img/laptop.jpg"
  alt: "laptop"
  credit: "XPS"
  source: "https://unsplash.com/photos/yNvVnPcurD8"
hero:
  size: halfheight
  embedTitle: false
  image:
    show: true
navBg: dark
navFg: light
---

# h1

## h2

### h3

#### h4

Some **bold** and *italic* text.

{{< text/message title="Sample" type="info" >}}
Sample info message with title **and** some *markdown*.

And a [link](http://example.com).
{{< /text/message >}}

{{< text/message type="warning" >}}
Sample warning message without tittle
{{< /text/message >}}

{{< text/message >}}
Sample message without title/type
{{< /text/message >}}

Outside the message block
