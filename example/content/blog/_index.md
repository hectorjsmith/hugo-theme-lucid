---
title: Blog
subtitle: All my blog posts
hero:
  background: primary
  size: halfheight
  embedTitle: true
navFg: light
---

You can find a list of my blog posts here:
